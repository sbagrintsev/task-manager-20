package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> mapByShort = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        final String name = command.getName();
        final String shortName = command.getShortName();
        if (name != null && !name.isEmpty()) mapByName.put(name, command);
        if (shortName != null && !shortName.isEmpty()) mapByShort.put(shortName, command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) throws CommandNotSupportedException {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException(name);
        final AbstractCommand command = mapByName.get(name);
        if (command == null) throw new CommandNotSupportedException(name);
        return command;
    }

    @Override
    public AbstractCommand getCommandByShort(final String shortName) throws ArgumentNotSupportedException {
        if (shortName == null || shortName.isEmpty()) throw new ArgumentNotSupportedException(shortName);
        final AbstractCommand command = mapByShort.get(shortName);
        if (command == null) throw new ArgumentNotSupportedException(shortName);
        return command;
    }

    @Override
    public Collection<AbstractCommand> getAvailableCommands() {
        return mapByName.values();
    }

}
