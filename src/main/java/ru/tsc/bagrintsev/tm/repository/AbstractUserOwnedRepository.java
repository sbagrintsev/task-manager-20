package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M record) {
        record.setUserId(userId);
        return add(record);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M m : records) {
            if (userId.equals(m.getUserId())) {
                result.add(m);
            }
        }
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M findOneById(final String userId, final String id) throws AbstractException {
        final List<M> userRecords = findAll(userId);
        for (final M record : userRecords) {
            if (record.getId().equals(id)) {
                return record;
            }
        }
        throw new ModelNotFoundException();
    }

    @Override
    public boolean existsById(final String userId, final String id) throws AbstractException {
        findOneById(userId, id);
        return true;
    }

    @Override
    public M remove(final String userId, final M record) {
        if (userId.equals(record.getUserId())) {
            records.remove(record);
        }
        return record;
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) throws ModelNotFoundException {
        final M record = findOneByIndex(userId, index);
        if (record == null) throw new ModelNotFoundException();
        return remove(userId, record);
    }

    @Override
    public M removeById(final String userId, final String id) throws AbstractException {
        final M record = findOneById(userId, id);
        return remove(userId, record);
    }

    @Override
    public int totalCount(final String userId) {
        int count = 0;
        for (final M record : records) {
            if (userId.equals(record.getUserId())) count++;
        }
        return count;
    }

    @Override
    public void clear(final String userId) {
        final List<M> records = findAll(userId);
        removeAll(records);
    }
}
