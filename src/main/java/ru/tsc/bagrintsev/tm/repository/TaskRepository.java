package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.*;


public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(userId, task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @Override
    public Map<Integer, Task> findAllByProjectId(final String projectId) {
        final Map<Integer, Task> taskMap = new TreeMap<>();
        for (int taskIndex = 0; taskIndex < totalCount(); taskIndex++) {
            final Task task = records.get(taskIndex);
            final String prjId = task.getProjectId();
            if (prjId == null) continue;
            if (prjId.equals(projectId)) {
                taskMap.put(taskIndex, task);
            }
        }
        return taskMap;
    }

    @Override
    public void setProjectId(final String userId, final String taskId, final String projectId) throws AbstractException {
        final Task task = findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

}
