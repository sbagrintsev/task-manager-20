package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectTaskService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Map;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository,
                              final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    private void checkIfEntityOK(final String projectId, final String taskId) throws AbstractException {
        check(EntityField.PROJECT_ID, projectId);
        check(EntityField.TASK_ID, taskId);
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        checkIfEntityOK(projectId, taskId);
        taskRepository.setProjectId(userId, taskId, projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String projectId, final String taskId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        checkIfEntityOK(projectId,taskId);
        taskRepository.setProjectId(userId, taskId, null);
    }
    @Override
    public void removeProjectById(final String userId, final String projectId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.PROJECT_ID, projectId);
        final Map<Integer, Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Map.Entry<Integer, Task> taskEntry : tasks.entrySet()) {
            final Task task = taskEntry.getValue();
            taskRepository.removeById(task.getId());
        }
        projectRepository.removeById(projectId);
    }

}
