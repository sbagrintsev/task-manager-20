package ru.tsc.bagrintsev.tm.command.system;

import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return new Role[0];
    }
}
