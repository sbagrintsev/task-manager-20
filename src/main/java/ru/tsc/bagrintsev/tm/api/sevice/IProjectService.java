package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Project;


public interface IProjectService extends IUserOwnedService<Project>{

    Project create(String userId, String name) throws AbstractException;

    Project create(String userId, String name, String description) throws AbstractException;

    Project updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Project updateById(String userId, String id, String name, String description) throws AbstractException;

    Project changeProjectStatusByIndex(String userId, Integer index, Status stauts) throws AbstractException;

    Project changeProjectStatusById(String userId, String id, Status status) throws AbstractException;

}
