package ru.tsc.bagrintsev.tm.exception.user;

import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;

public final class LoginIsIncorrectException extends AbstractUserException {

    public LoginIsIncorrectException() {
        super("Error! User with this login does not exist...");
    }

}
