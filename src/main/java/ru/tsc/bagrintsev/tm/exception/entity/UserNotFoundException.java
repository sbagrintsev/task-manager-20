package ru.tsc.bagrintsev.tm.exception.entity;

public final class UserNotFoundException extends AbstractEntityException{

    public UserNotFoundException() {
        super("Error! Search by null! User not found...");
    }

    public UserNotFoundException(String searchType, String identifier) {
        super(String.format("Error! User with %s: %s not found...", searchType, identifier));
    }

}
