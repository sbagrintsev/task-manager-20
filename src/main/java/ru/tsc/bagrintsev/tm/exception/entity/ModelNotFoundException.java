package ru.tsc.bagrintsev.tm.exception.entity;

import ru.tsc.bagrintsev.tm.model.AbstractModel;

public final class ModelNotFoundException extends AbstractEntityException{

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}
