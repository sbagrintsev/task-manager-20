package ru.tsc.bagrintsev.tm.exception.system;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException(String arg) {
        super(String.format("Error! Argument '%s' is not supported...", arg));
    }

}
