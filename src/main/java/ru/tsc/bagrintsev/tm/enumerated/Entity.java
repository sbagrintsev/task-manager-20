package ru.tsc.bagrintsev.tm.enumerated;

public enum Entity {
    ABSTRACT("abstract"),
    TASK("task"),
    PROJECT("project"),
    USER("user"),
    ROLE("role");

    private final String displayName;

    Entity(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
